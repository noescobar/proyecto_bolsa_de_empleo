from __future__ import unicode_literals

from django.db import models


class ActividadContrato(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=120)
    descripcion = models.CharField(max_length=400, blank=True)

    class Meta:
        db_table = 'empresa"."actividad_contrato'


class Competencia(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.IntegerField()
    descripcion = models.IntegerField()

    class Meta:
        db_table = 'empresa"."competencia'


class Contrato(models.Model):
    oferta = models.ForeignKey('Oferta', primary_key=True)
    salario = models.IntegerField()
    tipo_contrato = models.ForeignKey('TipoContrato')
    tipo_salario = models.ForeignKey('TipoSalario')
    jornada_trabajo = models.ForeignKey('JornadaTrabajo')
    requerimiento_especial = models.ForeignKey('RequerimientoEspecial')
    ciudad = models.ForeignKey('localizacionContacto.MunicipioCiudad')

    class Meta:
        db_table = 'empresa"."contrato'


class CorrecionesOfertas(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=120)
    descripcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'empresa"."correciones_ofertas'


class EstadoPublicacion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'empresa"."estado_publicacion'


class InformacionEmpresarial(models.Model):
    empresa = models.ForeignKey('seguridad.PerfilEmpresa', primary_key=True)
    correo_corporativo = models.CharField(max_length=120, blank=True)
    trabajadores = models.IntegerField(blank=True, null=True)
    naturaleza = models.CharField(max_length=30, blank=True)
    jerarquia = models.CharField(max_length=200, blank=True)
    actividad = models.CharField(max_length=150, blank=True)

    class Meta:
        db_table = 'empresa"."informacion_empresarial'


class JornadaTrabajo(models.Model):
    contrato_id = models.IntegerField(primary_key=True)
    hora_inicio = models.CharField(max_length=30)
    hora_finalizacion = models.CharField(max_length=30)
    tipo_jornada_id = models.IntegerField()

    class Meta:
        db_table = 'empresa"."jornada_trabajo'


class Oferta(models.Model):
    id = models.AutoField(primary_key=True)
    meses_experiencia = models.IntegerField(blank=True, null=True)
    habilidades = models.CharField(max_length=300, blank=True)
    vacantes_disponibles = models.IntegerField(blank=True, null=True)
    numero_postulantes_entrevistar = models.IntegerField()
    numero_contacto = models.IntegerField(blank=True, null=True)
    publicacion_id = models.IntegerField()
    contrato_id = models.IntegerField()

    class Meta:
        db_table = 'empresa"."oferta'


class OfertaTieneCompetencia(models.Model):
    oferta = models.ForeignKey(Oferta)
    competencia = models.ForeignKey(Competencia)

    class Meta:
        db_table = 'empresa"."oferta_tiene_competencia'


class OfertaTieneCorrecion(models.Model):
    oferta = models.ForeignKey(Oferta)
    correccion = models.ForeignKey(CorrecionesOfertas)
    fecha_correccion = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'empresa"."oferta_tiene_correcion'


class Publicacion(models.Model):
    id = models.AutoField(primary_key=True)
    oferta = models.ForeignKey(Oferta)
    fecha_publicacion = models.IntegerField()
    estado_publicacion = models.ForeignKey(EstadoPublicacion)

    class Meta:
        db_table = 'empresa"."publicacion'


class RecursoHumano(models.Model):
    empresa = models.ForeignKey('seguridad.PerfilEmpresa', primary_key=True)
    nombre_director = models.CharField(max_length=200)
    correo = models.CharField(max_length=120, blank=True)

    class Meta:
        db_table = 'empresa"."recurso_humano'


class RequerimientoEspecial(models.Model):
    id = models.AutoField(primary_key=True)
    manejo_personal = models.NullBooleanField(default=False)
    manejo_dinero = models.NullBooleanField(default=False)
    manejo_esquipos_especializados = models.NullBooleanField()
    libreta_militar = models.BooleanField(default=False)

    class Meta:
        db_table = 'empresa"."requerimiento_especial'


class TipoContrato(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'empresa"."tipo_contrato'


class TipoEmpresa(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=60)
    descripcion = models.CharField(max_length=400, blank=True)

    class Meta:
        db_table = 'empresa"."tipo_empresa'


class TipoJornadaTrabajo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=120)
    descripcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'empresa"."tipo_jornada_trabajo'


class TipoSalario(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'empresa"."tipo_salario'
