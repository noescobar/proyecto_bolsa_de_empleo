__author__ = 'noescobar'

from django.conf.urls import patterns, include, url
from django.contrib import admin
from views import loginView

urlpatterns = patterns('Backend.apps.seguridad.views',
                       
    url(r'^$', 'index_view', name='vista_principal'),
    url(r'^login/?$',loginView.as_view(),name='vista_login'),
    url(r'^logout/?$','logout_view', name='vista_logout'),
)
