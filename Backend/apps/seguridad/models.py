from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, UserManager



class Cuenta(User):
    fecha_creacion = models.DateTimeField()
    usuario = models.ForeignKey('Usuario')

    objects = UserManager()

    class Meta:
        db_table = 'seguridad"."cuenta'


class PerfilAdministrador(models.Model):
    id = models.ForeignKey(Cuenta, db_column='id', primary_key=True)

    class Meta:
        db_table = 'seguridad"."perfil_administrador'


class PerfilCiudadano(models.Model):
    id = models.ForeignKey(Cuenta, db_column='id', primary_key=True)
    hoja_de_vida = models.ForeignKey('ciudadano.HojaDeVida', unique=True, blank=True, null=True)

    class Meta:
        db_table = 'seguridad"."perfil_ciudadano'


class PerfilEmpresa(models.Model):
    id = models.ForeignKey(Cuenta, db_column='id', primary_key=True)
    numero_identifacion = models.CharField(unique=True, max_length=30)
    correo_corporativo = models.CharField(max_length=100)
    tipo_id = models.IntegerField()
    informacion_empresarial_id = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'seguridad"."perfil_empresa'


class PerfilFuncionario(models.Model):
    id = models.ForeignKey(Cuenta, db_column='id', primary_key=True)
    empresa = models.ForeignKey(PerfilEmpresa, unique=True)

    class Meta:
        db_table = 'seguridad"."perfil_funcionario'


class TerminoCondicion(models.Model):
    id = models.AutoField(primary_key=True) 
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    activo = models.BooleanField(default=False)

    class Meta:
        db_table = 'seguridad"."termino_condicion'


class Usuario(models.Model):
    id = models.AutoField(primary_key=True) 
    correo = models.CharField(unique=True, max_length=100)
    fecha_nacimiento = models.TimeField()
    nombre_completo = models.CharField(max_length=400)
    terminos_condiciones_aceptado = models.BooleanField(default=False)

    class Meta:
        db_table = 'seguridad"."usuario'
