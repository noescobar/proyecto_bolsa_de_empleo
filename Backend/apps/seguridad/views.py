from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from Backend.apps.seguridad.forms import LoginForm
from rest_framework.views import APIView
from rest_framework.views import Response
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from Config.settings import URL_LOGIN
from django.views.generic import base

def index_view(request):
    return render_to_response('index.html', context_instance=RequestContext(request))


@login_required(login_url=URL_LOGIN)
def login_view(request):
    mensaje = ""
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                next = request.POST['next']
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                usuario = authenticate(username=username, password=password)
                if usuario is not None and usuario.is_active:
                    login(request, usuario)
                    return HttpResponseRedirect(next)
                else:
                    mensaje = "usuario y/o password incorrecto"
        next = request.REQUEST.get('next')
        form = LoginForm()
        ctx = {'form': form, 'mensaje': mensaje, 'next': next}
        return render_to_response('login/login.html', ctx, context_instance=RequestContext(request))


class loginView(base.View):

    def get(self,request,*args,**kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:retornael temaplate con la informacion del usuario logueado o retorna el template  de logueo
        """
        form = LoginForm()
        ctx = {'formulario': form}
        print form.as_p()
        return render_to_response('login/login.html', ctx, context_instance=RequestContext(request))

    def post(self,request,*args,**kwargs):
        """

        :param request: username y un password
        :param args:
        :param kwargs:
        :return: retorna el template del index de un usuario logueado o retorna un template de error de logueo
        """
        form = LoginForm(request.POST)
        mensaje="formulario invalido"
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            usuario = authenticate(username=username, password=password)
            if usuario is not None and usuario.is_active:
                login(request, usuario)
                return HttpResponseRedirect("/")
            else:
                mensaje = "usuario y/o password incorrecto"
        return HttpResponse(mensaje)


    def delete(self,request,*args,**kwargs):
        logout(request)
        return HttpResponseRedirect('/')




def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')




