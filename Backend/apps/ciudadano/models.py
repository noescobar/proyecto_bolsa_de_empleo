from __future__ import unicode_literals

from django.db import models


class Capacitacion(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=100)
    estado = models.CharField(max_length=20, blank=True)
    fecha_inicio = models.DateField()
    fecha_finalizacion = models.DateField()
    soporte_logico = models.CharField(max_length=100)
    verificado = models.BooleanField(default=False)
    tiempo_cursado = models.TimeField(blank=True, null=True)
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion')

    class Meta:
        db_table = 'ciudadano"."capacitacion'


class EducacionPrimaria(models.Model):
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion', primary_key=True)
    nivel = models.CharField(max_length=20)
    graduado = models.BooleanField(default=True)
    fecha_graduacion = models.DateField()
    soporte_logico = models.CharField(max_length=200)
    verificado = models.BooleanField(default=False)

    class Meta:
        db_table = 'ciudadano"."educacion_primaria'


class EducacionSecundaria(models.Model):
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion', primary_key=True)
    nivel = models.CharField(max_length=20)
    graduado = models.BooleanField(default=True)
    fecha_graduacion = models.DateField()
    soporte_logico = models.CharField(max_length=200)
    verificado = models.BooleanField(default=False)

    class Meta:
        db_table = 'ciudadano"."educacion_secundaria'


class EducacionSuperior(models.Model):
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion', primary_key=True)
    nivel = models.CharField(max_length=20)
    graduado = models.BooleanField(default=True)
    fecha_graduacion = models.DateField()
    soporte_logico = models.CharField(max_length=200)
    verificado = models.BooleanField(default=False)

    class Meta:
        db_table = 'ciudadano"."educacion_superior'


class EstadoPostulacion(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'ciudadano"."estado_postulacion'


class ExperienciaLaboral(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre_institucion = models.CharField(max_length=60)
    ocupacion_cargo = models.IntegerField()
    fecha_incio = models.DateField()
    fecha_finalizacion = models.DateField()
    duracion = models.TimeField()
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion')

    class Meta:
        db_table = 'ciudadano"."experiencia_laboral'


class HojaDeVida(models.Model):
    perfil_ciudadano = models.ForeignKey('seguridad.PerfilCiudadano', primary_key=True)
    contenido = models.TextField()
    migrante = models.TextField()
    hoja_basica = models.ForeignKey('HojaDeVidaBasica', unique=True, blank=True, null=True)
    hoja_educacion = models.ForeignKey('HojaDeVidaEducacion', unique=True, blank=True, null=True)
    edad = models.IntegerField()
    tiempo_desempleado = models.TimeField()

    class Meta:
        db_table = 'ciudadano"."hoja_de_vida'


class HojaDeVidaBasica(models.Model):
    hoja_de_vida = models.ForeignKey(HojaDeVida, primary_key=True)
    informacion_personal = models.ForeignKey('InformacionPersonal', unique=True, blank=True, null=True)
    informacion_laboral = models.ForeignKey('InformacionLaboral', unique=True, blank=True, null=True)
    datos_de_contacto = models.ForeignKey('localizacionContacto.DatosDeContacto', unique=True, blank=True, null=True)
    sisben = models.ForeignKey('Sisben', unique=True, blank=True, null=True)
    residencia = models.ForeignKey('localizacionContacto.Residencia', blank=True, null=True)
    identificacion = models.ForeignKey('Identificacion', unique=True, blank=True, null=True)

    class Meta:
        db_table = 'ciudadano"."hoja_de_vida_basica'


class HojaDeVidaComentario(models.Model):
    hoja_vida = models.ForeignKey(HojaDeVida)
    perfil_funcionario = models.ForeignKey('seguridad.PerfilFuncionario')
    comentario = models.CharField(max_length=400)
    fecha = models.DateTimeField()

    class Meta:
        db_table = 'ciudadano"."hoja_de_vida_comentario'


class HojaDeVidaEducacion(models.Model):
    hoja_de_vida = models.ForeignKey(HojaDeVida, primary_key=True)
    trabajar_exterior = models.BooleanField(default=False)
    vive_exterior = models.BooleanField(default=False)
    educacion_primaria_id = models.IntegerField(unique=True, blank=True, null=True)
    educacion_secundaria_id = models.IntegerField(unique=True, blank=True, null=True)
    educacion_superior = models.ForeignKey(EducacionPrimaria, unique=True, blank=True, null=True)

    class Meta:
        db_table = 'ciudadano"."hoja_de_vida_educacion'


class Identificacion(models.Model):
    hoja_basica = models.ForeignKey(HojaDeVidaBasica,related_name="hoja_de_vida_tiene_identificacion", primary_key=True)
    tipo = models.ForeignKey('TipoIdentificacion')
    municipio_expedicion = models.ForeignKey('localizacionContacto.MunicipioCiudad')
    valor = models.IntegerField(unique=True)

    class Meta:
        db_table = 'ciudadano"."identificacion'


class IdiomaHablado(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    idioma = models.CharField(max_length=25)
    nivel_lectura = models.CharField(max_length=3)
    nivel_habla = models.CharField(max_length=3)
    nivel_escritura = models.CharField(max_length=3)
    nivel_escucha = models.CharField(max_length=3)
    soporte_logico = models.TextField()
    verificado = models.BooleanField(default=False)
    hoja_educacion = models.ForeignKey(HojaDeVidaEducacion)

    class Meta:
        db_table = 'ciudadano"."idioma_hablado'


class InformacionLaboral(models.Model):
    hoja_basica = models.ForeignKey(HojaDeVidaBasica, primary_key=True)
    personas_a_cargo = models.IntegerField()
    aspiracion_salarial = models.TextField()
    perfil_ocupacional = models.TextField()
    libreta_militar = models.CharField(max_length=200)

    class Meta:
        db_table = 'ciudadano"."informacion_laboral'


class InformacionPersonal(models.Model):
    hoja_basica = models.ForeignKey(HojaDeVidaBasica, primary_key=True)
    nombre = models.CharField(max_length=150, blank=True)
    primer_apellido = models.CharField(max_length=60)
    segundo_apellido = models.CharField(max_length=60, blank=True)
    genero = models.CharField(max_length=20, blank=True)
    estado_civil = models.CharField(max_length=100)
    posicion_familiar = models.CharField(max_length=60, blank=True)

    class Meta:
        db_table = 'ciudadano"."informacion_personal'


class InteresOcupacional(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre = models.CharField(max_length=100)
    hoja_educacion = models.ForeignKey(HojaDeVidaEducacion)

    class Meta:
        db_table = 'ciudadano"."interes_ocupacional'


class Logro(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    logro = models.CharField(max_length=200)
    descripcion = models.TextField(blank=True)
    informacion_laboral = models.ForeignKey(InformacionLaboral)

    class Meta:
        db_table = 'ciudadano"."logro'


class OcupacionLaboral(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre = models.CharField(max_length=40)
    descrpcion = models.CharField(max_length=400)

    class Meta:
        db_table = 'ciudadano"."ocupacion_laboral'


class Postulacion(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    fecha_postulacion = models.DateTimeField()
    estado = models.ForeignKey(EstadoPostulacion)
    perfil_ciudadano = models.ForeignKey('seguridad.PerfilCiudadano')
    publicacion = models.ForeignKey('empresa.Publicacion')

    class Meta:
        db_table = 'ciudadano"."postulacion'


class PracticaEmpresarial(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre_institucion = models.CharField(max_length=60)
    ocupacion_cargo = models.CharField(max_length=70)
    fecha_inicio = models.DateField()
    fecha_inalizacion = models.DateField()
    duracion = models.TimeField()
    hoja_educacion = models.ForeignKey(HojaDeVidaEducacion)

    class Meta:
        db_table = 'ciudadano"."practica_empresarial'


class Sisben(models.Model):
    hoja_basica_id = models.IntegerField(primary_key=True)
    nombre = models.IntegerField()
    tipo = models.IntegerField()
    nivel = models.IntegerField()
    puntaje = models.IntegerField()
    municipio_id = models.IntegerField()
    fecha_afiliacion = models.ForeignKey('localizacionContacto.MunicipioCiudad', db_column='fecha_afiliacion',
                                         blank=True, null=True)

    class Meta:
        db_table = 'ciudadano"."sisben'


class TipoIdentificacion(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()

    class Meta:
        db_table = 'ciudadano"."tipo_identificacion'
