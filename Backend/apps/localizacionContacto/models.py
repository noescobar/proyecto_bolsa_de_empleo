from __future__ import unicode_literals

from django.db import models
from django.utils import timezone



class DatosDeContacto(models.Model):
    id = models.AutoField(primary_key=True)
    numero_telefonico = models.IntegerField(blank=True, null=True)
    numero_celular = models.IntegerField(blank=True, null=True)
    extension = models.IntegerField(blank=True, null=True)
    indicativo_pais = models.IntegerField()
    indicativo = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'localizacion_y_contacto"."datos_de_contacto'


class Departamento(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    pais = models.ForeignKey('Pais')

    class Meta:
        db_table = 'localizacion_y_contacto"."departamento'


class MunicipioCiudad(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    departamento = models.ForeignKey(Departamento)

    class Meta:
        db_table = 'localizacion_y_contacto"."municipio_ciudad'


class Pais(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.IntegerField()

    class Meta:
        db_table = 'localizacion_y_contacto"."pais'


class Residencia(models.Model):
    id = models.AutoField(primary_key=True)
    barrio = models.CharField(max_length=120, blank=True)
    direccion = models.CharField(max_length=100, blank=True)
    estrato = models.IntegerField()
    ciudad = models.ForeignKey(MunicipioCiudad)

    class Meta:
        db_table = 'localizacion_y_contacto"."residencia'
