from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User, BaseUserManager, UserManager

class Cuenta(User):
    """
        username esta embebido dentro de la clase User
        passworld esta embebido dentro de la clase User
    """
    usuario = models.ForeignKey('Usuario', db_column='usuario_id', null=True)
    correo = models.EmailField(db_column='correo', blank=True, null=True)

    objects = UserManager()

    def __str__(self):
        return str(self.correo)+":"+str(self.usuario)

    class Meta:
        db_table = 'seguridad"."cuenta'


class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    primer_nombre = models.CharField(max_length=50)
    segundo_nombre = models.CharField(max_length=50)
    primer_apellido = models.CharField(max_length=50)
    segundo_apellido = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.id) + " : " + str(self.first_name)

    class Meta:
        db_table = 'seguridad"."usuario'
