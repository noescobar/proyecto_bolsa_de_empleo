# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cuenta',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('correo', models.EmailField(max_length=75, null=True, db_column='correo', blank=True)),
            ],
            options={
                'db_table': 'seguridad"."cuenta',
            },
            bases=('auth.user',),
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('primer_nombre', models.CharField(max_length=50)),
                ('segundo_nombre', models.CharField(max_length=50)),
                ('primer_apellido', models.CharField(max_length=50)),
                ('segundo_apellido', models.CharField(max_length=50)),
                ('fecha_nacimiento', models.DateField(auto_now_add=True)),
            ],
            options={
                'db_table': 'secury"."usuario',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='cuenta',
            name='usuario',
            field=models.ForeignKey(db_column='usuario_id', to='secury.Usuario', null=True),
            preserve_default=True,
        ),
    ]
