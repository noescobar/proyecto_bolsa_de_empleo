from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from Config.settings import FRONTEND_URL
from Config import settings

#from Backend.apps.secury.views import UsuriosViews

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'PruebaLab.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^seguridad/', include('Backend.apps.seguridad.urls')),
    #url(r'^/media/(?P<path>.*)$','django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    #url(r'^/?$', UsuriosViews.as_view()),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  #+ static('/Frontend/', document_root=FRONTEND_URL)

